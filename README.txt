﻿Версия Java: 1.8.0_131

Инструкция по развертыванию:

I Общая информация

1. https сервер работает на 3000 порту и проксирует запросы на 6060 порт основного приложения
2. Swagger Api документация доступна по адресу http://localhost:6060/swagger-ui.html

II Подготовка базы данных

Настройки подключения базы данных находятся в файле
src/main/resources/spring/database.properties

1. Установить PostgreSQL 9.6
2. Выполнить createdb.sql скрипт(в корне проекта) из PostgreSQL
консоли под супер пользователем:
psql -U postgres -a -f createdb.sql

Скрипт создаст базу данных "crypting" и нового пользователя "crypter" с правами для доступа к этой бд.

III Инструменты для сборки проекта
В проекте используются Gradle(бекенд) и Gulp(фронтенд) сборщики проектов.

Для того, чтобы собрать и развернуть проект, необходимо:
1. установить gradle, если еще не установлен.
   1) перейти по ссылке https://services.gradle.org/distributions/gradle-4.9-bin.zip
   чтобы скачать архив с gradle
   2) распаковать в необходимую директорию
   3) установить в переменную окружения PATH(Linux) или Path(Win) путь <gradle-path>/bin
2. перейти в корень проекта
3. выполнить из терминала gradle wrapper
4. затем выполнить ./gradlew clean build(Linux) или gradlew.bat clean build(Win)
5. и затем выполнить java -jar build/libs/crypting-1.0.0.jar
6. перейти в директорию frontend/ и выполнить следующие команды:
   1) npm i
   2) bower i
   3) gulp watch-dev
7. в браузере открыть https://localhost:3000/
8. На странице приложения есть ссылка "Как войти по ЭП",
перейдя по которой можно прочесть описание того, как получить тестовые сертификаты
и какие настройки в системе необходимо осуществить, чтобы с ними работать.

Для того, чтобы получить тестовые сертификаты, перейдите по ссылке https://www.cryptopro.ru/sites/default/files/products/cades/demopage/main.html и далее перейдите по ссылке
"Проверить работу установленного плагина" или по ссылке "Cертификат ключа подписи, который можно получить на странице тестового центра".

IV Использование сертификационного центра для проверки валидности пользовательских сертификатов

Для чего это надо? Проводить полные проверки по валидности сертификатов: отозван ли сертификат, находится ли в списке доверенных и др.
В тестовых целях эта функция отключена, т.к. для получения или обновления данных по сертификатам
из авторизованного центра сертификации потребуется не менее часа.

Все свойства, описанные ниже находятся в application.properties

Для включения функции первоначальной загрузки данных и доверенного сертификационного центра, необходимо установить свойство app.run.ca.update.while.starting в значение true

Для того, чтобы включить возможность проверки пользовательских сертификатов, используя сертификационный центр следующие свойства необходимо установить в значение true:
1. update.certifications.job.is.switch.on
2. app.validate.user.certs.by.ca.strictly

V Установка JCP

В корне проекта находится файл jcp-2.0.39442.zip archive in the root of the project that you need to unzip anywhere.
1. распаковать архив jcp-2.0.39442.zip, который находится в корне проекта в любую директорию в системе
2. перейти в распакованную директорию
3. выполнить "sudo ./setup_console.sh <path-to-jdk>"(Linux)
или "setup_console.bat <path-to-jdk>"(Win)
4. следовать указаниям утилиты по установке(все выбирать по умолчанию)

VI Работа с приложением

1. На основном экране приложения выбрать сертификат из списка
2. Нажать на кнопку Вход в личный кабинет после закрытия окон оповещения, которые предлагает CryptoPro
    Если мы увидели в правом верхнем углу ошибку "Сертификат не найден в системе", то:
3. Нажать на ссылку "Зарегистрируйтесь"
4. В новом окне в выпадающем списке снова выбрать сертификат, который хотите зарегистрировать в системе
5. Нажать на кнопку Зарегистрироваться и должно появиться оповещение Успешно: Вы зарегистрировались в системе
6. Нажать на крестик напротив слова Регистрация, чтобы снова перейти на окно авторизации
7. Повторить первые два пункта.

Должно появиться оповещение "Вы вошли в систему", что означает, что сертификат был успешно зарегистрирован в системе, сертификат прошел все этапы валидации и система допустила владельца сертификата к ее использованию.
